
// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
function card (pictures, title, info, sum, count) {
    return {
        pictures: pictures,
        title: title,
        info: info,
        sum: sum,
        count: count 
    }   
}

let a = card("pictures.jpg", 'gamburger' , 'lorem ipsum' , 40000, 1)
let b = card("pictures2.jpg", 'Lavash' , 'lorem' , 20000, 1)
let c = card("pictures3.jpg", 'pizza' , 'lorem ipsum doler' , 80000, 1)
// You code here ...
console.log(a);
console.log(b);
console.log(c);

// Constructor Function
// You code here ...
function Product (pictures, title, info, sum, count) {
    this.pictures = pictures;
    this.title = title;
    this.info = info;
    this.sum = sum;
    this.count = count 
}


// create objects
const cardbox1 = new Product("pictures.jpg", 'cheesburger' , 'lorem ipsum' , 30000, 1);
console.log(cardbox1);
const cardbox2 = new Product("pictures.jpg", 'donar' , 'lorem ipsum' , 25000, 1);
console.log(cardbox2);
const cardbox3 = new Product("pictures.jpg", 'sandwich' , 'lorem ipsum' , 2000, 1);
console.log(cardbox3);

