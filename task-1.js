// Convert the switch statement into an object called lookup.
// Use it to look up val and assign the associated string to the result variable.

// ! You should not use case, switch, or if statements

// Setup
function phoneticLookup(val) {
  let result = '';

  // Only change code below this line
  let lookup = {
    alpha: 'Adams',
    bravo: 'Boston',
    charlie: 'Chicago',
    delta: 'Denver',
    echo: 'Easy',
    foxtrot: 'Frank'
  }
  result = lookup[val];
  // Only change code above this line
  return result;
}

phoneticLookup('charlie'); // Chicago
phoneticLookup('alpha'); // Adams
phoneticLookup('bravo'); // Boston
phoneticLookup(''); // undefined
console.log(phoneticLookup('charlie'));
console.log(phoneticLookup('alpha'));
console.log(phoneticLookup('bravo'));
console.log(phoneticLookup(''));